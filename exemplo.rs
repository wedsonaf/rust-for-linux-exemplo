//! Exemplo simplificado do scull do livro Linux Device Drivers, terceira edição.

use kernel::file::File;
use kernel::file_operations::FileOperations;
use kernel::io_buffer::{IoBufferReader, IoBufferWriter};
use kernel::prelude::*;
use kernel::sync::{Mutex, Ref, RefBorrow, UniqueRef};
use kernel::{bindings, miscdev};

module! {
    type: Exemplo,
    name: b"exemplo",
    license: b"GPL v2",
    params: {
        nr_devs: i32 {
            default: 1,
            permissions: 0o644,
            description: b"Numero de dispositivos",
        },
    },
}

struct Conteudo {
    dados: Vec<u8>,
}

struct Dispositivo {
    numero: usize,
    conteudo: Mutex<Conteudo>,
}

impl Dispositivo {
    fn try_new(numero: usize) -> Result<Ref<Self>> {
        let mut disp = Pin::from(UniqueRef::try_new(Dispositivo {
            numero,
            // SAFETY: `mutex_init` é chamada abaixo.
            conteudo: unsafe { Mutex::new(Conteudo { dados: Vec::new() }) },
        })?);

        // SAFETY: `conteudo` está fixado quando o dispositivo está.
        let m = unsafe { disp.as_mut().map_unchecked_mut(|d| &mut d.conteudo) };
        kernel::mutex_init!(m, "Dispositivo::conteudo");
        Ok(disp.into())
    }
}

struct Exemplo {
    _devs: Vec<Pin<Box<miscdev::Registration<Exemplo>>>>,
}

impl FileOperations for Exemplo {
    type OpenData = Ref<Dispositivo>;
    type Wrapper = Ref<Dispositivo>;

    kernel::declare_file_operations!(read, write);

    fn open(data: &Ref<Dispositivo>, file: &File) -> Result<Ref<Dispositivo>> {
        pr_info!("Dispositivo {} está sendo aberto\n", data.numero);
        if file.flags() & bindings::O_ACCMODE == bindings::O_WRONLY {
            data.conteudo.lock().dados.clear();
        }
        Ok(data.clone())
    }

    fn read(
        this: RefBorrow<'_, Dispositivo>,
        _file: &File,
        data: &mut impl IoBufferWriter,
        offset: u64,
    ) -> Result<usize> {
        let offset = offset.try_into()?;
        let guard = this.conteudo.lock();
        let len = core::cmp::min(data.len(), guard.dados.len().saturating_sub(offset));
        data.write_slice(&guard.dados[offset..][..len])?;
        Ok(len)
    }

    fn write(
        this: RefBorrow<'_, Dispositivo>,
        _file: &File,
        data: &mut impl IoBufferReader,
        offset: u64,
    ) -> Result<usize> {
        let offset = offset.try_into()?;
        let len = data.len();
        let new_len = len.checked_add(offset).ok_or(Error::EINVAL)?;
        let mut guard = this.conteudo.lock();
        if new_len > guard.dados.len() {
            guard.dados.try_resize(new_len, 0)?;
        }
        data.read_slice(&mut guard.dados[offset..][..len])?;
        Ok(len)
    }
}

impl KernelModule for Exemplo {
    fn init(_name: &'static CStr, module: &'static ThisModule) -> Result<Self> {
        let count = {
            let lock = module.kernel_param_lock();
            (*nr_devs.read(&lock)).try_into()?
        };
        pr_info!("Olá mundo, {} dispositivos!\n", count);
        let mut devs = Vec::try_with_capacity(count)?;
        for i in 0..count {
            let disp = Dispositivo::try_new(i)?;
            devs.try_push(miscdev::Registration::new_pinned(fmt!("exemplo{i}"), disp)?)?;
        }
        Ok(Exemplo { _devs: devs })
    }
}

impl Drop for Exemplo {
    fn drop(&mut self) {
        pr_info!("Adeus mundo!\n");
    }
}
